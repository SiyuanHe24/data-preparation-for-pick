from pdf2image import convert_from_path
import pytesseract
from PIL import Image
import cv2
import numpy as np
import csv
import os

from IPython import embed

def get_data(img):
    
    data = pytesseract.image_to_data(img)
    
    data = data.split('\n')

    for idx, item in enumerate(data):
        data[idx] = item.split('\t')
        data[idx][-1] = data[idx][-1].lstrip().rstrip() 
    
    return list(filter(lambda item: item[-1] != '', data))[1:]

def concat_words(data):

    formatted_data = []

    idx = 0
    transcript = ''
    x1 = int(data[0][6])
    y1 = int(data[0][7])
    x2 = x1 + int(data[0][8])
    y2 = y1 + int(data[0][9])
    
    def differentRow(idx, data):
        return (int(data[idx][5]) <= int(data[idx - 1][5]))

    def tooFar(idx, data):

        distance = abs(int(data[idx][6]) - (int(data[idx - 1][6]) + int(data[idx - 1][8])))
        width = max(int(data[idx][8]) / len(data[idx][-1]), int(data[idx - 1][8]) / len(data[idx][-1]))

        return (distance > 5 * width)

    while (idx < len(data)):
        
        if ((idx > 0) and (differentRow(idx, data) or tooFar(idx, data))):

            formatted_data.append([data[idx][2], x1, y1, x2, y2, transcript[1:]])
            transcript = ''
            x1 = int(data[idx][6])
            y1 = int(data[idx][7])
            y2 = int(data[idx][7]) + int(data[idx][9])

        transcript = transcript + ' ' + data[idx][-1]
        y1 = min(y1, int(data[idx][7]))
        x2 = int(data[idx][6]) + int(data[idx][8])
        y2 = max(y2, int(data[idx][7]) + int(data[idx][9]))

        idx += 1

    formatted_data.append([data[idx - 1][2], x1, y1, x2, y2, transcript])

    return formatted_data

def draw_bbx(img, data):
    
    img_bbx = np.float32(img)
    
    for block in data:
        img_bbx = cv2.rectangle(img_bbx, (block[1], block[2]), (block[3], block[4]), (255, 0, 0), 2)
        
    return img_bbx

def save_png(filename, img, dir_png_folder):
    cv2.imwrite('%s\\%s.png'%(config['dir_png_folder'], filename), np.float32(img))

def save_img(filename, data, dir_tsv_folder):
    with open('%s\\%s.tsv'%(dir_tsv_folder, filename), 'w') as output:
        for item in data:

            item_str = str(item[0]) + ','

            item_str = item_str + str(item[1]) + ','
            item_str = item_str + str(item[2]) + ','
            item_str = item_str + str(item[3]) + ','
            item_str = item_str + str(item[2]) + ','
            item_str = item_str + str(item[3]) + ','
            item_str = item_str + str(item[4]) + ','
            item_str = item_str + str(item[1]) + ','
            item_str = item_str + str(item[4]) + ','
            item_str = item_str + str(item[-1]) + ','
            
            output.write(item_str + '\n')

def extract_info(img, filename, config):
    
    save_png(filename, img, config['dir_png_folder'])

    data = get_data(img)

    data = concat_words(data)

    img_bbx = draw_bbx(img, data)

    cv2.imwrite('%s\\%s.png'%(config['dir_bbx_folder'], filename), img_bbx)

    save_img(filename, data, config['dir_tsv_folder'])

if __name__=='__main__':
    
    config = {
        'dir_input_folder' : '.\\samples',
        'dir_bbx_folder' : '.\\bbx_output',
        'dir_tsv_folder' : '.\\tsv_output',
        'dir_png_folder' : '.\\png_output',
    }

    for filename in os.listdir(config['dir_input_folder']):
        dir_pdf = os.path.join(config['dir_input_folder'], filename)
        png_imgs = convert_from_path(dir_pdf, 500)
        
        extract_info(png_imgs[0], filename[:-4], config)
   
    embed()
    






    

    


    

    


    embed()
   